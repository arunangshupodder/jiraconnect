import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import static com.jira.utilities.Constants.*;
import static com.jira.utilities.GenericUtils.*;

import com.jira.scripts.TestExecution;

public class CreateTestExec {

	public static void main(String[] args) 
	{
		try 
		{
			initProcess();
			
			File file = new File(TC_FILE);
			Workbook wb = WorkbookFactory.create(file);
			Sheet sheet = wb.getSheet(TC_SHEET); 
			
			Row row = sheet.getRow(0);
			Cell cell;
			for (int i=0; i<row.getLastCellNum(); i++)
			{
				cell = row.getCell(i);
				
				if (cell==null)
				{
					
				}
				else
				{
					String cellValue = cell.getStringCellValue();
					
					if (cellValue.equalsIgnoreCase(JIRAKEY))
						jiraKeyColumnIndex = i; 
					else if (cellValue.equalsIgnoreCase(EXECUTION_STATUS))
						statusColumnIndex = i;
				}
			}
			
			for (int i=1; i<=sheet.getLastRowNum(); i++)
			{
				row = sheet.getRow(i);
				
				Map<String, String> dataMap = new HashMap<String, String>();
				dataMap.put(TESTCASE_ID, row.getCell(jiraKeyColumnIndex).getStringCellValue());
				dataMap.put(STATUS, row.getCell(statusColumnIndex).getStringCellValue());
				
				String testExecutionId = TestExecution.createIssueTest(dataMap);
				System.out.println("Test execution id created: " + testExecutionId);
				dataMap.put(TESTEXECUTION_ID, testExecutionId);
				
				TestExecution.createTestRun(dataMap);
				
				writeToOutputFile(dataMap.get(TESTCASE_ID), testExecutionId);
			}
		} 
		catch (EncryptedDocumentException | InvalidFormatException | IOException e) 
		{
			e.printStackTrace();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			closeOutputFile();
		}
	}

}
