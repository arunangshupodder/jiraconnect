package com.jira.utilities;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Properties;

import static com.jira.utilities.Constants.*;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class EncryptCredentials 
{
	Cipher ecipher;
	Cipher dcipher;
	
	EncryptCredentials(SecretKey key) throws Exception 
	{
	    ecipher = Cipher.getInstance("DES");
	    dcipher = Cipher.getInstance("DES");
	    ecipher.init(Cipher.ENCRYPT_MODE, key);
	    dcipher.init(Cipher.DECRYPT_MODE, key);
	}
	
	public String encrypt(String str) throws Exception 
	{
	    // Encode the string into bytes using utf-8
	    byte[] utf8 = str.getBytes("UTF8");
	    // Encrypt
	    byte[] enc = ecipher.doFinal(utf8);
	    // Encode bytes to base64 to get a string
	    return new sun.misc.BASE64Encoder().encode(enc);
	}

	public String decrypt(String str) throws Exception 
	{
	    // Decode base64 to get bytes
	    byte[] dec = new sun.misc.BASE64Decoder().decodeBuffer(str);

	    byte[] utf8 = dcipher.doFinal(dec);

	    // Decode using utf-8
	    return new String(utf8, "UTF8");
	}
	
	public static void main(String[] args) 
	{
		Properties prop = new Properties();
		InputStream input = null;
		OutputStream output = null;
		
		SecretKey key;
		try 
		{
			input = new FileInputStream(CREDS_FILE);
			prop.load(input);
			
			String id = prop.getProperty("id");
			String pwd = prop.getProperty("password");
			
			if (pwd.endsWith("=="))
			{
				System.out.println("Password already encrypted");
				//System.out.println(decryptCreds());
				return;
			}
				
			key = KeyGenerator.getInstance("DES").generateKey();
			EncryptCredentials encrypter = new EncryptCredentials(key);
		    String encrypted = encrypter.encrypt(pwd);
		    
		    String encodedKey = Base64.getEncoder().encodeToString(key.getEncoded());
		    
		    prop.setProperty("id", id);
		    prop.setProperty("password", encrypted);
		    prop.setProperty("key", encodedKey);
		    
		    output = new FileOutputStream(CREDS_FILE);
		    prop.store(output, null);
		  
		    
		    //System.out.println(decryptCreds());
		} 
		catch (NoSuchAlgorithmException e) 
		{
			e.printStackTrace();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}	
	}
}
