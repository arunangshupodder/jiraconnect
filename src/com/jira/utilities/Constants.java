package com.jira.utilities;

import java.text.SimpleDateFormat;
import java.util.Date;

import io.restassured.RestAssured;

public class Constants 
{
	//PROPERTIES FILES
	public static final String CREDS_FILE = ".//src//com//jira//utilities//Credential.properties";
	
	
	//OUTPUT FILE DETAILS
	public static final String executionTime;
	static
	{
		 SimpleDateFormat formatter = new SimpleDateFormat("dd_MM_yyyy HH_mm_ss");  
		 Date date = new Date();  
		 executionTime = formatter.format(date);  
	}
	public static final String OUTPUT_FILE = ".//src//com//jira//utilities//output_" + executionTime + ".txt";
	
	
	//EXCEL FILE DETAILS
	public static final String TC_FILE = "C://Users//Arunangshu//Desktop//file.xlsx";
	public static final String TC_SHEET = "Sheet1";
	
	//EXCEL COLUMN INDEXES
	public static int jiraKeyColumnIndex;
	public static int statusColumnIndex;
	
	//EXCEL FILE COLUMNS
	public static final String JIRAKEY = "JIRAKEY";
	public static final String EXECUTION_STATUS = "Result";
	
	
	//DATA MAP KEY IDS
	public static final String TESTCASE_ID = "tcId";
	public static final String STATUS = "status";
	public static final String TESTEXECUTION_ID = "testExecId";
	
	
	//JIRA CONSTANTS
	public static final String PROJECT_ID = "RT";
	public static final String TESTPLAN = "RT-1";
	public static final String ISSUE_TESTEXECUTION = "Test Execution";
	public static final String FIXVERSION = null;
	public static final String IT_TEAM = null;
	
	public static final String EXECUTION_SUMMARY = "Sample Test Execution";
	public static final String EXECUTION_DESCRIPTION = "example of manual test";
	
	
	//URLS
	static
	{
		RestAssured.baseURI = "http://localhost:8080/rest/api/2/issue";
	}
	public static final String JIRA_BASE_URL = "http://localhost:8080/rest/api/2/issue";
	public static final String XRAY_BASE_URL = "http://localhost:8080/rest/raven/1.0/import/execution";
	
	
	
	
	
	
}