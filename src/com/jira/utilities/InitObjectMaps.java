package com.jira.utilities;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.jira.objectmaps.CustomField10007Map;
import com.jira.objectmaps.CustomField10011Map;
import com.jira.objectmaps.ExecutionResultMap;
import com.jira.objectmaps.FieldsMap;
import com.jira.objectmaps.IssueTypeMap;
import com.jira.objectmaps.ProjectMap;
import com.jira.objectmaps.TestMap;
import com.jira.objectmaps.TestRunMap;
import com.jira.objectmaps.TestStepMap;

import static com.jira.utilities.Constants.*;

public class InitObjectMaps 
{
	public static TestMap initMapTestRequest(Map<String, String> dataMap)
	{
		TestStepMap teststep;
		List<TestStepMap> s = new ArrayList<TestStepMap>();
		
		for (int i=0; i<4; i++)
		{
			teststep = new TestStepMap();
			
			int index = i;
			String step = "Step " + (i+1);
			String data = "Data " + (i+1);
			String result = "Result " + (i+1);
			
			teststep.setIndex(index);
			teststep.setStep(step);
			teststep.setData(data);
			teststep.setResult(result);
			
			s.add(i, teststep);
		}
		
		CustomField10011Map customfield_10011 = new CustomField10011Map();
		customfield_10011.setSteps(s);
		
		CustomField10007Map customfield_10007 = new CustomField10007Map();
		customfield_10007.setValue("Manual");
		
		IssueTypeMap issuetype = new IssueTypeMap();
		issuetype.setName("Test");
		
		ProjectMap project = new ProjectMap();
		project.setKey("RT");
		
		
		
		
		FieldsMap fields = new FieldsMap();
		fields.setProject(project);
		fields.setSummary(dataMap.get("tc_id"));
		fields.setDescription("example of manual test");
		fields.setIssuetype(issuetype);
		fields.setCustomfield_10007(customfield_10007);
		fields.setCustomfield_10011(customfield_10011);
		
		
		
		TestMap request = new TestMap();
		request.setFields(fields);
		
		
		
		return request;
	}
	
	
	
	
	
	public static TestMap initMapTestExecRequest(Map<String, String> dataMap)
	{
		List<String> testPlan = new ArrayList<String>();
		testPlan.add(TESTPLAN);
		
		List<String> test = new ArrayList<String>();
		test.add(dataMap.get(TESTCASE_ID));
		
		IssueTypeMap issuetype = new IssueTypeMap();
		issuetype.setName(ISSUE_TESTEXECUTION);
		
		List<String> fixVersions = new ArrayList<String>();
		fixVersions.add(FIXVERSION);
		
		List<String> itTeam = new ArrayList<String>();
		itTeam.add(IT_TEAM);
		
		ProjectMap project = new ProjectMap();
		project.setKey(PROJECT_ID);
		
		FieldsMap fields = new FieldsMap();
		fields.setProject(project);
		fields.setSummary(EXECUTION_SUMMARY);
		fields.setDescription(EXECUTION_DESCRIPTION);
		fields.setIssuetype(issuetype);
		//fields.setFixVersions(fixVersions);
		//fields.setCustomfield_12479(itTeam);
		fields.setCustomfield_10034(testPlan);		//change for test plan
		fields.setCustomfield_10022(test);		//change for test
		
		TestMap request = new TestMap();
		request.setFields(fields);
		
		return request;
	}
	
	public static TestRunMap initMapTestRunRequest(Map<String, String> dataMap)
	{
		ExecutionResultMap erm = new ExecutionResultMap();
		erm.setTestKey(dataMap.get(TESTCASE_ID));
		if (dataMap.get(STATUS).equalsIgnoreCase("PASS"))
		{
			erm.setStatus("PASS");
		}
		else if (dataMap.get(STATUS).equalsIgnoreCase("FAIL"))
		{
			erm.setStatus("FAIL");
		}
		else
		{
			erm.setStatus("TODO");
		}
		/*
		String date = java.time.LocalDateTime.now().toString();
		erm.setStartedOn(date);
		erm.setFinishedOn(date);
		*/
		List<ExecutionResultMap> list = new ArrayList<ExecutionResultMap>();
		list.add(erm);
		
		TestRunMap object = new TestRunMap();
		object.setTestExecutionKey(dataMap.get(TESTEXECUTION_ID));
		object.setTests(list);
		
		return object;
	}
}
