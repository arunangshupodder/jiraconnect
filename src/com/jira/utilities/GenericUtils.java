package com.jira.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import static com.jira.utilities.Constants.*;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Row.MissingCellPolicy;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;


public class GenericUtils 
{
	private static File tcFile;
	private static Workbook workbook;
	private static Sheet sheet;
	
	private static FileWriter fileWriter;
	
	public static List<Integer> testCaseRowIndex; 
	
	private static int testCaseIdColPosition;
	private static int testCaseDescColPosition;
	
	public static String getPassword() throws Exception
	{
		SecretKey key;
		InputStream input = null;
		Properties prop = new Properties();
		
		input = new FileInputStream(CREDS_FILE);
		prop.load(input);
		
		byte[] decodedKey = Base64.getDecoder().decode(prop.getProperty("key"));
		key = new SecretKeySpec(decodedKey, 0, decodedKey.length, "DES"); 
		
		EncryptCredentials decrypter = new EncryptCredentials(key);
		
		String decrypted = decrypter.decrypt(prop.getProperty("password"));
	    //System.out.println(decrypted);
	    
	    return decrypted;
	}
	
	public static String getId() throws IOException
	{
		InputStream input = null;
		Properties prop = new Properties();
		
		input = new FileInputStream(CREDS_FILE);
		prop.load(input);
		
		return prop.getProperty("id");
	}
	
	public static Map<String, String> getTestCase(int testCaseNumber)
	{
		Map<String, String> dataMap = new HashMap<String, String>();
		
		try
		{
			 int testCaseStartIndex = testCaseRowIndex.get(testCaseNumber);  
             Row r = sheet.getRow(testCaseStartIndex);
             Cell c;
             
             c = r.getCell(testCaseIdColPosition, MissingCellPolicy.RETURN_BLANK_AS_NULL);
             if (!(c == null))
            	 dataMap.put("tc_id", c.getStringCellValue()); 
             
             
             
             c = r.getCell(testCaseDescColPosition, MissingCellPolicy.RETURN_BLANK_AS_NULL);
             if (!(c == null))
            	 dataMap.put("tc_desc", c.getStringCellValue());
             
             /*
             for (int i=1; i<sheet.getLastRowNum(); i++)
             {
            	 r = sheet.getRow(i);
            	 
            	 c = r.getCell(testCaseIdColPosition, MissingCellPolicy.RETURN_BLANK_AS_NULL);
            	 if (!(c == null))
            	 {
            		 dataMap.put("tc_id", c.getStringCellValue());
            		 testCaseStartIndex = i;
            	 }
             }*/
		}
		catch(NullPointerException e)
		{
			System.out.println("No more test cases found in sheet " + sheet.getSheetName());
			//e.printStackTrace();
		}
		catch(Exception e)
		{
			throw e;
		}
		
		return dataMap;

	}
	
	private static int getColumnPosition(Row r, String colName)
	{
		Cell c;
		
		for (int i=0; i<r.getLastCellNum(); i++)
        {
       	 c = r.getCell(i);
       	 
       	 if (c.getStringCellValue().equalsIgnoreCase(colName))
       		 return i;
        }
		
		return -1;
	}
	
	public static void initTestCaseFile(String filePath) throws EncryptedDocumentException, InvalidFormatException, IOException
	{
		tcFile = new File(filePath); 
		workbook = WorkbookFactory.create(tcFile);
	}
	
	public static int getNumberOfSheets()
	{
		return workbook.getNumberOfSheets();
	}
	
	public static void getSheet(int sheetIndex)
	{
		sheet = workbook.getSheetAt(sheetIndex);
		System.out.println("Reading sheet => " + sheet.getSheetName());
		
		searchColumnsInSheet();
	}
	
	private static void searchColumnsInSheet()
	{
		Row r = sheet.getRow(0);
		
		testCaseIdColPosition = getColumnPosition(r, "Test Case ID");
		testCaseDescColPosition = getColumnPosition(r, "Test Case Description");
	}
	
	public static int getTestCaseCount()
	{
		testCaseRowIndex = new ArrayList<Integer>();
		
		try
		{
             Row r;
             Cell c;
             
             for (int i=1; i<sheet.getLastRowNum(); i++)
             {
            	 r = sheet.getRow(i);    	 
            	 c = r.getCell(testCaseIdColPosition, MissingCellPolicy.RETURN_BLANK_AS_NULL);
            	 if (!(c == null))
            		 testCaseRowIndex.add(i);
            		 
             }
		}
		catch(NullPointerException e)
		{
			System.out.println("No more test cases found in sheet " + sheet.getSheetName());
		}
		catch(Exception e)
		{
			throw e;
		}
		
		return testCaseRowIndex.size();
	}
	
	public static void writeToOutputFile(String testId, String testExecId) throws IOException
	{
		fileWriter.write("Test Case ID: " + testId + " Test Execution ID: " + testExecId);
		fileWriter.write("\r\n");// write new line
	}
	
	public static void closeOutputFile()
	{
		try 
		{
			fileWriter.close();
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void initProcess()
	{
		try 
		{
			//initTestCaseFile(filePath);
			fileWriter = new FileWriter(OUTPUT_FILE, true);
		} 
		catch (EncryptedDocumentException | IOException e) 
		{
			e.printStackTrace();
		}
	}
}
