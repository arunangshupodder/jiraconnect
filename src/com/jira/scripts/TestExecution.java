package com.jira.scripts;

import static com.jira.utilities.GenericUtils.*;
import static com.jira.utilities.InitObjectMaps.*;
import static io.restassured.RestAssured.*;
import static com.jira.utilities.Constants.*;

import java.io.IOException;
import java.net.URI;
import java.util.Map;

import com.jira.objectmaps.TestMap;
import com.jira.objectmaps.TestRunMap;

import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class TestExecution 
{
	public static String createIssueTest(Map<String, String> dataMap) throws IOException, Exception
	{
		TestMap request = initMapTestExecRequest(dataMap);
		
		Response response = given()
				  .log().body()
				  .auth().preemptive().basic(getId(), getPassword())
				  .header("X-Atlassian-Token", "no-check")
				  .accept(ContentType.JSON).with().contentType(ContentType.JSON).and().body(request)
				  .when()
				  .post(new URI(JIRA_BASE_URL));
		
		int statusCode = response.thenReturn().statusCode();
		System.out.println("Test Execution Status Code: " + statusCode);
		
		String jiraId;
		if(statusCode == 200 || statusCode == 201)
		{
			jiraId = response.jsonPath().getString("key");
		}
		else
		{
			System.out.println(response.asString());
			jiraId = Integer.toString(-1);
		}

		return jiraId;
	}
	
	public static void createTestRun(Map<String, String> dataMap) throws IOException, Exception
	{
		TestRunMap request = initMapTestRunRequest(dataMap);
		
		Response response = given()
				  .log().body()
				  .auth().preemptive().basic(getId(), getPassword())
				  .header("X-Atlassian-Token", "no-check")
				  .accept(ContentType.JSON).with().contentType(ContentType.JSON).and().body(request)
				  .when()
				  .post(new URI(XRAY_BASE_URL));
		
		System.out.println(response.thenReturn().statusCode());		  
		
		//String jiraId = response.getString("key");
	}
}
