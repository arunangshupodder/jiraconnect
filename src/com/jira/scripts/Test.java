package com.jira.scripts;

import java.io.File;
import java.util.Map;
import java.util.Scanner;

import com.jira.objectmaps.TestMap;
import com.jira.utilities.Constants;

import static com.jira.utilities.GenericUtils.*;
import static com.jira.utilities.InitObjectMaps.*;

import static io.restassured.RestAssured.*;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;

public class Test extends Constants {
	
	public static String createIssueTest(Map<String, String> dataMap) throws Exception
	{
		TestMap request = initMapTestRequest(dataMap);
		
		JsonPath response = given()
						  .log().body()
						  .auth().preemptive().basic(getId(), getPassword())
						  .header("X-Atlassian-Token", "no-check")
						  .accept(ContentType.JSON).with().contentType(ContentType.JSON).and().body(request)
						  .when()
						  .post()
						  .thenReturn()
						  .jsonPath();
		
		//System.out.println(response.prettyPrint());
		String jiraId = response.getString("key");
		
		return jiraId;
	}
	
	public static void getIssueTest(String issue) throws Exception
	{
		JsonPath response = given()
						  .auth().preemptive().basic(getId(), getPassword())
						  .when()
						  .get("/" + issue)
						  .thenReturn()
						  .jsonPath();
		
		System.out.println(response.prettyPrint());
	}
	
}
