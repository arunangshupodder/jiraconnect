package com.jira.sample;

import java.io.FileWriter;
import java.io.IOException;

public class ClassA 
{
	public static void main(String[] args)
	{
		try {
            FileWriter writer = new FileWriter("C://Users//Arunangshu//Desktop//MyFile.txt", true);
            writer.write("Hello World");
            writer.write("Good Bye!");
            writer.write("\r\n");// write new line
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
	}
}
