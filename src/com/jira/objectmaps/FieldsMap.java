package com.jira.objectmaps;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class FieldsMap {
	private ProjectMap project;
	private String summary;
	private String description;
	private IssueTypeMap issuetype;
	private CustomField10007Map customfield_10007;
	private CustomField10011Map customfield_10011;
	private List<String> customfield_12479;
	private List<String> fixVersions;
	
	private List<String> customfield_10034;
	private List<String> customfield_10022;
	
	
	public ProjectMap getProject() {
		return project;
	}
	public void setProject(ProjectMap project) {
		this.project = project;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public IssueTypeMap getIssuetype() {
		return issuetype;
	}
	public void setIssuetype(IssueTypeMap issuetype) {
		this.issuetype = issuetype;
	}
	public CustomField10007Map getCustomfield_10007() {
		return customfield_10007;
	}
	public void setCustomfield_10007(CustomField10007Map customfield_10007) {
		this.customfield_10007 = customfield_10007;
	}
	public CustomField10011Map getCustomfield_10011() {
		return customfield_10011;
	}
	public void setCustomfield_10011(CustomField10011Map customfield_10011) {
		this.customfield_10011 = customfield_10011;
	}
	public List<String> getCustomfield_10034() {
		return customfield_10034;
	}
	public void setCustomfield_10034(List<String> customfield_10034) {
		this.customfield_10034 = customfield_10034;
	}
	public List<String> getCustomfield_10022() {
		return customfield_10022;
	}
	public void setCustomfield_10022(List<String> customfield_10022) {
		this.customfield_10022 = customfield_10022;
	}
	public List<String> getFixVersions() {
		return fixVersions;
	}
	public void setFixVersions(List<String> fixVersions) {
		this.fixVersions = fixVersions;
	}
	public List<String> getCustomfield_12479() {
		return customfield_12479;
	}
	public void setCustomfield_12479(List<String> customfield_12479) {
		this.customfield_12479 = customfield_12479;
	}
	
}
