package com.jira.objectmaps;

public class ExecutionResultMap {
	private String testKey;
	private String status;
	//private String startedOn;
	//private String finishedOn;
	
	public String getTestKey() {
		return testKey;
	}
	public void setTestKey(String testKey) {
		this.testKey = testKey;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	/*
	public String getStartedOn() {
		return startedOn;
	}
	public void setStartedOn(String startedOn) {
		this.startedOn = startedOn;
	}
	public String getFinishedOn() {
		return finishedOn;
	}
	public void setFinishedOn(String finishedOn) {
		this.finishedOn = finishedOn;
	}*/
}
