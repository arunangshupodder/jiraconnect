package com.jira.objectmaps;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class TestRunMap 
{
	private String testExecutionKey;
	//private InfoMap info;
	private List<ExecutionResultMap> tests;
	
	
	public String getTestExecutionKey() {
		return testExecutionKey;
	}
	public void setTestExecutionKey(String testExecutionKey) {
		this.testExecutionKey = testExecutionKey;
	}
	public List<ExecutionResultMap> getTests() {
		return tests;
	}
	public void setTests(List<ExecutionResultMap> tests) {
		this.tests = tests;
	}
}
