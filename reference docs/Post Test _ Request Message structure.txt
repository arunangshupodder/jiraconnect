{
    "fields": {
       "project":
       {
          "key": "RT"
       },
       "summary": "Sum of two numbers",
       "description": "example of manual test",
       "issuetype": 
	   {
          "name": "Test"
       },
       "customfield_10007": { "value": "Manual" },             
       "customfield_10011": 
	   {
            "steps": [
                {
                    "index": 0,
                    "step": "Step 1",
                    "data": "input Data 1",
                    "result": "Excepted result 1"
                },
                {
                    "index": 1,
                    "step": "Step 2",
                    "data": "input Data 2",
                    "result": "Excepted result 2"                     
                },
                {
                    "index": 2,
                    "step": "Step 3",
                    "data": "input Data 3",
                    "result": "Excepted result 3"                     
                },
                {
                    "index": 3,
                    "step": "Step 4",
                    "data": "input Data 4",
                    "result": "Excepted result 4"                     
                }
            ]
        }
   }
}